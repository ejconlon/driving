all: test run

run:
	python solution.py

test:
	py.test -v --capture=no test_solution.py
