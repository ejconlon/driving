Driver Code Challenge
=====

Eric Conlon
2016-12-07


Running
-----

Requires Python 3:

    pip install -r requirements.txt
    make all


Design
-----

I've reproduced the problem statement in `SPEC.md`, so I won't repeat too much.  First, some shorthand: Let N be the number of subsequences, and M the charatcter length of the longest subsequence.  (We are working with N subsequences of M characters each.)

Let's start by examining the the dumbest possible solution that could work: depth-first search with linear substring matching. Start with any subsequence, then for every other subsequence, compare all suffixes (of over half length) of the first against the second.  For every match, (of over half length of the second), push the first, and recurse.  The time complexity of this is simply horrible: it's factorial in N since we're basically searching through permutations.  That is certainly a good justification for proposing a more complex algorithm!

We need to narrow our search space of permutations of subsequences.  One way to do that is to produce a comes-before graph, then topologically sort it.  The time complexity of the sort linear in the number of subsequences and the number of comes-before relations between them, in the worst case O(N^2).  In most real cases we expect the number of edges to be very small due to the high entropy of the letter distribution and the requirement that subsequences overlap over half their lengths.

We could produce this comes-before graph in a number of ways.  The first is by doing subsequence-to-subsequence comparisons.  This is O(N^2) time multiplied by the complexity of the search, which is O(M^3) in the naive case or O(M^2) if you're smarter and use Boyer-Moore or the like. Another alternative is to build a prefix trie in O(N * M^2) time and space (fairly easy) or a prefix tree in O(N * M) time and space (trickier).  I think the prefix tree method is probably the way to go, but for simplicity's sake I tried a trie first.  On my old laptop I was able to solve the given example with a trie in ~10 seconds with ~40MB of memory so I wasn't pressed to optimize further.


Implementation and Testing
-----

I did this in Python, but you should find that it's quite amenable to static typing (just read the docstrings for the types).  `solution.py` contains all the relevant code for parsing, solving, and running.  `datasets` contains both example inputs and outputs.  I unit tested a bunch of the functions I used and also wrote a generator for these kinds of problems that really helped to catch corner cases --  give `test_solution.py` a quick read.  One thing to note is that the generated problems don't necessarily have unique solutions, so I generate all solutions and simply assert that one of them is the expected solution.
