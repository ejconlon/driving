#!/usr/bin/env python


"""
See SPEC.md for the problem description.

See `solve` for the algorithm description.

See `main` for instructions on running it.
"""


from pprint import pprint
import sys


# For validation and test data generation, fix our alphabet
ALPHABET = 'ACGT'

# This is the default file we read from
DEFAULT_FILENAME = 'datasets/challenge/question.txt'

# If True, lots of things will be printed.
VERBOSE = False

# If True, lots of things will be tested.
SANITY = True


def vprint(*s):
  """
  print when VERBOSE
  """
  if VERBOSE:
    print(*s, file=sys.stderr)


def vpprint(*s):
  """
  pprint when VERBOSE
  """
  if VERBOSE:
    pprint(*s, file=sys.stderr)


def read_fasta(filename):
  """
  Reads pairs (name, sequence) from a FASTA-like file.

  In the real world, think twice about rolling your own parser. Silly corner cases abound.

  Args:
    filename (str): path to FASTA-like file

  Returns:
    (generator) of
      (tuple) of
        name (str): key (like 'Rosalind_XYZ')
        sequence (str): value (like 'GCTA')
  """
  with open(filename, 'r') as f:
    name = None
    seq = ''
    line = f.readline()
    while len(line) > 0:
      line = line.strip()
      if len(line) > 0:
        if line.startswith('>'):
          # names delimit values
          if len(seq) > 0:
            assert name is not None
            yield (name, seq)
            name = None
            seq = ''
          name = line[1:]
        elif line.startswith(';'):
          # comments delimit values
          if len(seq) > 0:
            assert name is not None
            yield (name, seq)
            name = None
            seq = ''
        else:
          assert name is not None
          # check that we're reading only valid chars
          for c in line:
            assert c in ALPHABET
          seq += line
      else:
        # blank lines delimit values
        if len(seq) > 0:
          assert name is not None
          yield (name, seq)
          name = None
          seq = ''
      line = f.readline()
    # EOFs delimit values
    if len(seq) > 0:
      assert name is not None
      yield (name, seq)
      name = None
      seq = ''

def assert_can_be_connected(pairs):
  """
  A sanity check that sserts that these pairs can be connected at all. There
  should be at most one element that has no suffix-matches; likewise for
  prefixes. (That is to say: there cannot be two elements that can only end
  (likewise only begin) the joined sequence.)

  Args:
    pairs ([(str, str)]): FASTA key-value pairs
  """
  num_start = 0
  num_end = 0
  # For each (k, v) in pairs, assert that 
  for k0, v0 in pairs:
    # 
    min_overlap = len(v0) // 2 + 1
    num_prefix = 0
    num_suffix = 0
    for k1, v1 in pairs:
      if k0 == k1:
        continue
      f = v0.find(v1[:min_overlap])
      if f >= 0:
        num_suffix += 1
      g = v1.find(v0[:min_overlap])
      if g >= 0:
        num_prefix += 1
    if num_suffix == 0:
      vprint('end only:', k0)
      num_end += 1
    if num_prefix == 0:
      vprint('start only:', k0)
      num_start += 1
  vprint('num start:', num_start)
  vprint('num end:', num_end)
  assert num_start <= 1
  assert num_end <= 1


class Trie:
  """
  Dumb trie. Doesn't do anything fancy with prefixes; instead allocates one
  node for every item in the key sequence. Leaves store sets of values.

  TODO encode common prefixes better. no sense having tons of single-child
  nodes, but hey, heap's cheap for this problem.
  """

  @classmethod
  def empty(cls):
    """
    Use this to make a new, empty Trie
    """
    return Trie({}, set())

  def __init__(self, children, values):
    """
    Use `empty` above.

    Params:
      children (dict[char, Trie]): pointers to next level of the trie
      values (set[str]): values for keys ending here
    """
    assert children is not None
    assert values is not None
    self.children = children
    self.values = values

  def insert(self, key, value):
    """
    Inserts key -> value, building intermediate nodes as it goes.

    Params:
      key (str)
      value (str)
    """
    node = self  # iterative descent
    while len(key) > 0:
      if key[0] in node.children:
        node = node.children[key[0]]
      else:
        child = Trie.empty()
        node.children[key[0]] = child
        node = child
      key = key[1:]
    node.values.add(value)

  def lookup(self, key):
    """
    Looks up values by key.

    Params:
      key (str)

    Returns:
      (set[str]) values assoc with that key, empty if key not in trie
    """
    node = self  # iterative descent
    while len(key) > 0:
      if key[0] in node.children:
        node = node.children[key[0]]
      else:
        return set()
      key = key[1:]
    return node.values


def vpprint_trie(trie):
  """
  Pretty-prints a trie if VERBOSE. Don't spend too much time reading this.

  Args:
    trie (Trie)
  """
  if VERBOSE:
    def p(indent, *s):
      if indent == 0:
        vprint(*s)
      else:
        vprint(' ' * (indent - 1), *s)
    stack = [('root', trie, 0)]
    while len(stack) > 0:
      title, cur, indent = stack.pop()
      p(indent, title + ':')
      indent += 2
      if cur.children is None:
        if cur.tail is None:
          assert len(cur.values) == 0
          p(indent, 'empty')
        else:
          p(indent, 'values:', cur.values)
          p(indent, 'tail:', cur.tail)
      else:
        p(indent, 'values:', cur.values)
        p(indent, 'children:')
        for k, v in cur.children.items():
          stack.append((k, v, indent + 2))


def gen_suffixes(key):
  """
  Generates suffixes of a given string up to half its length.

  Args:
    key (str)

  Returns:
    (generator) of
      (tuple) of
        offset (int) number of chars dropped from start of string (can be 0)
        suffix (str)
  """
  for i in range(0, (len(key) + 1) // 2):
    yield (i, key[i:])


def gen_prefixes(key):
  """
  Generates prefixes of a given string up to half its length.

  Args:
    key (str)

  Returns:
    (generator) of
      (tuple) of
        offset (int) number of chars dropped from end of string (can be 0)
        prefix (str)
  """
  for i in range(0, (len(key) + 1) // 2):
    if i == 0:
      yield (i, key)
    else:
      yield (i, key[:-i])


class OverlapTrie:
  """
  A poor programmer's suffix trie (or prefix trie, as the case may be).

  "Looks like" a Trie, but inserts prefixes and looks up suffixes.
  """

  def __init__(self):
    self.trie = Trie.empty()

  def insert(self, key, value):
    """
    Insert prefix -> value for all prefixes of key (see `gen_prefixes`)

    Args:
      key (str)
      value (str)
    """
    for i, k in gen_prefixes(key):
      self.trie.insert(k, value)

  def lookup(self, key):
    """
    Lookup all suffixes of key and return values along with offsets
    (see `gen_suffixes`).

    Args:
      key (str)

    Returns:
      (set[(int, str)]) union of (offset, value) pairs
    """
    s = set()
    for i, k in gen_suffixes(key):
      for t in self.trie.lookup(k):
        s.add((i, t))
    return s


def toposub(d, rev, pairs, stack, seen, k):
  """
  See `toposort` - this is a recursive helper
  """
  assert(k in seen)
  k_value_len = len(pairs[rev[k]][1])
  for (i, w) in d[k]:
    if w not in seen:
      w_value_len = len(pairs[rev[w]][1])
      overlap = k_value_len - i
      # by construction we should only have things in the map
      # that overlap by more than half
      if overlap < k_value_len // 2 or overlap < w_value_len // 2:
        raise Exception('overlap too small', overlap, k, k_value_len, w, w_value_len)
      stack.append((i, k))
      seen.add(w)
      if len(seen) == len(d):
        # -1 is a sentinel value here (see `join`)
        stack.append((-1, w))
        yield stack
        stack.pop()
      else:
        yield from toposub(d, rev, pairs, stack, seen, w)
      seen.remove(w)
      stack.pop()


def toposort(d, rev, pairs):
  """
  Toposort a 'comes-before' map to recover an ordering of sequence keys
  and offsets that can join to produce the original (full) sequence.

  Args:
    d (dict[str, [(int, str)]]): 'comes-before' map of key to (offset, key)
    rev (dict[str, int]): key lookup for pairs
    pairs ([(str, str)]): (key, value) pairs

  Returns:
    (generator) of
      ([(int, str)]) offset-key pairs
  """
  for k in d.keys():
    yield from toposub(d, rev, pairs, [], set([k]), k)


def join(ks, rev, pairs):
  """
  Join a sequence of offsets and keys to recover the original (full) sequence.

  Args:
    ks ([(int, str)]): offset-key pairs
    rev (dict[str, int]): key lookup for pairs
    pairs ([(str, str)]): (key, value) pairs

  Returns:
    (str) the original (full) sequence
  """
  s = ''
  for (i, k) in ks:
    v = pairs[rev[k]][1]
    # -1 is a sentinel value for the end of the list
    # (meaning drop nothing, just end with this)
    if i > 0:
      s += v[:i]
    else:
      s += v
  return s


def solve(pairs):
  """
  The guts of the thing.

  Args:
    pairs ([(str, str)]): FASTA-like key-value pairs

  Returns:
    (generator) of
      (tuple) of
        ks ([(int, str)]): offset-key pairs
        full (str): original (full) sequence
  """

  # Go through our pairs once:
  # insert prefixes into the trie
  # and setup our reverse lookup of keys to indices
  ot = OverlapTrie()
  i = 0
  rev = {}
  for (k, v) in pairs:
    vprint('len', k, len(v))
    ot.insert(v, k)
    rev[k] = i
    i += 1

  # Fill in our 'comes-before' map
  d = {}
  for (k, v) in pairs:
    t = ot.lookup(v)
    # exclude that a key comes-before itself
    u = [x for x in t if x[1] != k]
    # sort to bias search toward smaller offsets (larger overlaps)
    u.sort(key = lambda x: x[0])
    d[k] = u

  if VERBOSE:
    vpprint(list(sorted(list(d.items()))))

  # Toposort the 'comes-before' map and yield all solutions 
  for ks in toposort(d, rev, pairs):
    vpprint(ks)
    full = join(ks, rev, pairs)    
    yield (ks, full)


def main():
  """
  Entry point to the program.

  Run with `-h` for help.

  Run with no args to process the default file.
  """
  use_string = "python solution.py [FILENAME]"
  if len(sys.argv) == 1:
    filename = DEFAULT_FILENAME
  elif len(sys.argv) == 2:
    if sys.argv[1] in ['-h', '--help']:
      print('Use `{}`'.format(use_string), file=sys.stderr)
      sys.exit(0)
    filename = sys.argv[1]
  else:
    print('Too many arguments. Use `{}`'.format(use_string), file=sys.stderr)
    sys.exit(-1)
  pairs = list(read_fasta(filename))
  if SANITY:
    assert_can_be_connected(pairs)
  # just pick the first solution here
  _, solution = next(solve(pairs))
  print(solution)


if __name__ == '__main__':
  main()
