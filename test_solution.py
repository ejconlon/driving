"""
Various tests.
"""


import itertools
import random

from solution import *


# A simple test case
EXAMPLE_PAIRS = [
  ('Frag_56', 'ATTAGACCTG'),
  ('Frag_57', 'CCTGCCGGAA'),
  ('Frag_58', 'AGACCTGCCG'),
  ('Frag_59', 'GCCGGAATAC')
]

# And its answer
EXAMPLE_SOLUTION = 'ATTAGACCTGCCGGAATAC'


def test_read():
  """
  Test that we can read a FASTA-like file
  """
  pairs = list(read_fasta(DEFAULT_FILENAME))
  assert len(pairs) == 50
  ks = set()
  vs = set()
  min_len = None
  max_len = None
  for (k, v) in pairs:
    vlen = len(v)
    assert vlen > 0
    assert vlen <= 1000
    if min_len is None or vlen < min_len:
      min_len = vlen
    if max_len is None or vlen > max_len:
      max_len = vlen
    assert k not in ks
    assert v not in vs
    ks.add(k)
    vs.add(v)
  assert min_len == 980
  assert max_len == 1000


def test_trie():
  """
  Test that we can do basic inserts and lookups on a Trie
  """
  t = Trie.empty()
  assert t.lookup('123') == set()
  t.insert('123', 'a')
  assert t.lookup('123') == set('a')
  assert t.lookup('12') == set()
  assert t.lookup('1234') == set()
  t.insert('123', 'b')
  assert t.lookup('123') == set(['a', 'b'])
  t.insert('145', 'c')
  assert t.lookup('123') == set(['a', 'b'])
  assert t.lookup('145') == set('c')
  t.insert('67', 'd')
  assert t.lookup('123') == set(['a', 'b'])
  assert t.lookup('145') == set('c')
  assert t.lookup('67') == set('d')


def test_gen_suffixes():
  """
  Test that we generate suffixes up to half of the length of the input.
  """
  f = lambda s: list(gen_suffixes(s))
  assert f('') == []
  assert f('1') == [(0, '1')]
  assert f('12') == [(0, '12')]
  assert f('123') == [(0, '123'), (1, '23')]
  assert f('1234') == [(0, '1234'), (1, '234')]
  assert f('12345') == [(0, '12345'), (1, '2345'), (2, '345')]
  assert f('123456') == [(0, '123456'), (1, '23456'), (2, '3456')]
  assert f('1234567') == [(0, '1234567'), (1, '234567'), (2, '34567'), (3, '4567')]


def test_gen_prefixes():
  """
  Test that we generate prefixes up to half of the length of the input.
  """
  f = lambda s: list(gen_prefixes(s))
  assert f('') == []
  assert f('1') == [(0, '1')]
  assert f('12') == [(0, '12')]
  assert f('123') == [(0, '123'), (1, '12')]
  assert f('1234') == [(0, '1234'), (1, '123')]
  assert f('12345') == [(0, '12345'), (1, '1234'), (2, '123')]
  assert f('123456') == [(0, '123456'), (1, '12345'), (2, '1234')]
  assert f('1234567') == [(0, '1234567'), (1, '123456'), (2, '12345'), (3, '1234')]


def test_simple():
  """
  Test that we find a solution for the simple example
  """
  parsed = list(read_fasta('datasets/simple/question.txt'))
  assert parsed == EXAMPLE_PAIRS
  with open('datasets/simple/answer.txt', 'r') as f:
    answer = f.read().strip()
  assert answer == EXAMPLE_SOLUTION
  assert_can_be_connected(EXAMPLE_PAIRS)
  found = False
  for _, solution in solve(EXAMPLE_PAIRS):
    if solution == EXAMPLE_SOLUTION:
      found = True
      break
  assert found


def gen_seqs(min_seq_len, max_seq_len, num_seqs):
  """
  Generate sequences to really test our algorithm.

  (It ain't pretty.)

  Params:
    min_seq_len (int): minimum length of any individual sequence
    max_seq_len (int): maximum length of any individual sequence
    num_seqs (int): number of sequences

  Returns:
    (tuple) of
      pairs ([(str, str)]): FASTA-like pairs of keys and values (in random order)
      full (str): full sequence
  """
  slen = random.randint(min_seq_len, max_seq_len)
  cur = ''.join(random.choice(ALPHABET) for j in range(slen))
  full = cur
  seqs = [cur]
  while len(seqs) < num_seqs:
    offset = None
    new_max = None
    # TODO come up with the closed form to this (but it works fine for now)
    while True:
      # come up with an offset from the start of the last sequence
      # (must be <= half)
      if len(cur) % 2 == 0:
        end = len(cur) // 2 - 1
      else:
        end = len(cur) // 2
      offset = random.randint(1, end)
      # length of the overlap between this and the next sequence
      overlap = len(cur) - offset
      assert offset <= len(cur) // 2
      assert overlap > len(cur) // 2
      # given this overlap, we need to bound the size of the next
      # sequence so it covers more than half of it
      new_max = overlap * 2 - 1
      if new_max >= min_seq_len:
        # found an offset that works, break and use it
        break
    # come up with the next sequence len
    slen = random.randint(min_seq_len, min(max_seq_len, new_max))
    assert overlap > slen // 2
    # leave only the shared suffix...
    cur = cur[offset:]
    # ... and generate the remaining characters
    remaining = slen - len(cur)
    if remaining > 0:
      added = ''.join(random.choice(ALPHABET) for j in range(remaining))
      cur += added
      full += added
    seqs.append(cur)
  # add dummy keys 'seq_0', ...
  indexed = [('seq_{}'.format(i), seqs[i]) for i in range(len(seqs))]
  # shuffle to better exercise our algorithm
  random.shuffle(indexed)
  return (indexed, full)


def is_gen_ordered(ks):
  """
  Params:
    ks ([(int, str)]): (offset, key) pairs

  Returns:
    (bool) are the pairs in the same order as generated?
  """
  for i in range(len(ks)):
    if ks[i][1] != 'seq_{}'.format(i):
      return False
  return True


def test_with_gen_seqs():
  """
  Generate a bunch of test data and assert `solve` works correctly.
  """
  # set a fixed seed here if you hit an error
  seed = random.randint(0, sys.maxsize)
  print('using seed', seed)
  random.seed(seed)
  for i in range(1000):
    vprint('testing', i)
    # generate some pairs and a solution
    seqs, full = gen_seqs(10, 15, 10)
    vprint(seqs)
    vprint(full)
    # assert it can be solved in the first place
    assert_can_be_connected(seqs)
    found = False
    # we're not guaranteed one solution, so assert we find the one we're
    # looking for among all the ones we find
    for ks, solution in solve(seqs):
      if solution == full and is_gen_ordered(ks):
        found = True
    assert found
